package com.example.labo1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class accueil extends AppCompatActivity {
    private String[] ActiviteList;
    private AlertDialog ad;
    private String temp;
    private ArrayAdapter<String> listAdapter;
    private ListView list1;
    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       workerAllActivite dbw = new workerAllActivite(this);
        dbw.execute("1","123");
        SharedPreferences myprefs = getSharedPreferences("SHARED_PREFS",MODE_PRIVATE);
        temp = myprefs.getString("list_key","");
        ActiviteList= temp.split(",");
        final List<String> listact = new ArrayList<String>();
        Collections.addAll(listact, ActiviteList);
        listact.remove(listact.size()-1);
        listAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listact);
        list1 = (ListView)findViewById(R.id.listview);
        list1.setAdapter(listAdapter);
        registerForContextMenu(list1);

        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Toast.makeText(getApplicationContext(),"You just selected: " + ((TextView)view).getText().toString(),Toast.LENGTH_LONG).show();

            }
        });

//        list1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//             //ajouter menu
//                registerForContextMenu(list1);
//                listAdapter.notifyDataSetChanged();
//                return  true;
//            }
//        });
        //Toast.makeText(this, "le id est: "+listact.get(1) , Toast.LENGTH_LONG).show();

    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
       MenuInflater inflater = getMenuInflater();
       inflater.inflate(R.menu.menuactivite,menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.option1:
                Intent Facteur = new Intent(this,activity.class);
                Facteur.putExtra("message",item.getItemId());
                startActivity(Facteur);
                return true;

            case R.id.option2:
                Intent Facteur2 = new Intent(this,AddActivity.class);
                //Facteur2.putExtra("mesage",msg.getText().toString());
                startActivity(Facteur2);
                return true;
        }

        Toast.makeText(this, "test"+ item.getItemId(), Toast.LENGTH_SHORT).show();

        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.accueil_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.profil:
                Intent Facteur = new Intent(this,modification.class);
                //Facteur.putExtra("mesage",msg.getText().toString());
                startActivity(Facteur);
                return true;

            case R.id.newact:
                Intent Facteur2 = new Intent(this,AddActivity.class);
                //Facteur2.putExtra("mesage",msg.getText().toString());
                startActivity(Facteur2);
                return true;

                case R.id.disconnect:
                Intent Facteur3 = new Intent(this,MainActivity.class);
                //Facteur2.putExtra("mesage",msg.getText().toString());
                startActivity(Facteur3);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    }
