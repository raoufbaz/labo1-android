package com.example.labo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class AddActivity extends AppCompatActivity {
    private EditText nom;
    private EditText desc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        nom = (EditText)findViewById(R.id.nom);
        desc = (EditText)findViewById(R.id.desc);
    }

    public void NewActivite(View view){

        String userN = this.nom.getText().toString();
        String userP = this.desc.getText().toString();

        workerAddAct dbw = new workerAddAct(this);

        dbw.execute(userN,userP);
        Intent Facteur = new Intent(this,accueil.class);
        //Facteur.putExtra("mesage",msg.getText().toString());
        startActivity(Facteur);
    }
}
