package com.example.labo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class modification extends AppCompatActivity {

    private EditText user;
    private EditText pw;
    private String id;
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modification);
        user = (EditText)findViewById(R.id.newusername);
        pw = (EditText)findViewById(R.id.newpwd);

        SharedPreferences myprefs = getSharedPreferences("SHARED_PREFS",MODE_PRIVATE);
        id = myprefs.getString("id_key","");
        //Toast.makeText(this, "le id est: "+id , Toast.LENGTH_LONG).show();

    }

    public void Comptemodif(View view){

        String userN = this.user.getText().toString();
        String userP = this.pw.getText().toString();

        workerModif dbw = new workerModif(this);


        dbw.execute(id,userN,userP);
    }
}
