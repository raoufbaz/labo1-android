package com.example.labo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

public class activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity);
        SharedPreferences myprefs = getSharedPreferences("SHARED_PREFS",MODE_PRIVATE);
        String id = myprefs.getString("id_key","");
        Intent intent = getIntent();
        String nom = intent.getStringExtra("message");

        workerSignUpAct dbw = new workerSignUpAct(this);
        dbw.execute(id,nom);
    }
}
