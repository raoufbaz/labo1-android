package com.example.labo1;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText txtUser;
    private EditText txtPw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtPw = (EditText)findViewById(R.id.userpw);
        txtUser = (EditText)findViewById(R.id.userName);
    }

    public void logUserIn(View v){
        String userN = this.txtUser.getText().toString();
        String userP = this.txtPw.getText().toString();

        dbWorker dbw = new dbWorker(this);
        dbw.execute(userN,userP);

    }

    public void inscription(View view) {

        Intent Facteur = new Intent(this,inscription.class);
        //Facteur.putExtra("mesage",msg.getText().toString());
        startActivity(Facteur);
    }

    public void goToAccueil(){
        Intent Facteur = new Intent(this,accueil.class);
        //Facteur.putExtra("mesage",msg.getText().toString());
        startActivity(Facteur);
    }

}
