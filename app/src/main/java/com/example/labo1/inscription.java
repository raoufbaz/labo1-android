package com.example.labo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class inscription extends AppCompatActivity {

    private EditText txtnom;
    private EditText txtprenom;
    private EditText txttravail;
    private EditText txtuser;
    private EditText txtpwd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        txtnom = (EditText)findViewById(R.id.txtnom);
        txtprenom = (EditText)findViewById(R.id.txtprenom);
        txttravail = (EditText)findViewById(R.id.txttravail);
        txtuser = (EditText)findViewById(R.id.txtuser);
        txtpwd = (EditText)findViewById(R.id.txtpwd);
    }

    public void dbinscription(View view) {

        String userNom = this.txtnom.getText().toString();
        String userPrenom = this.txtprenom.getText().toString();
        String userTravail = this.txttravail.getText().toString();
        String userUsername = this.txtuser.getText().toString();
        String userPWD = this.txtpwd.getText().toString();

        WorkerInscription dbw = new WorkerInscription(this);
        dbw.execute(userNom,userPrenom,userTravail,userUsername,userPWD);

       // Intent Facteur = new Intent(this,MainActivity.class);
       // startActivity(Facteur);
    }

}
