package com.example.labo1;

        import android.app.Activity;
        import android.app.AlertDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.icu.util.Output;
        import android.os.AsyncTask;
        import android.widget.Toast;
        import java.io.BufferedReader;
        import java.io.BufferedWriter;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.io.OutputStream;
        import java.io.OutputStreamWriter;
        import java.net.HttpURLConnection;
        import java.net.URL;
        import java.net.URLEncoder;

        import static android.content.Context.MODE_PRIVATE;

class workerAllActivite extends AsyncTask {

    private Context c;
    private AlertDialog ad;
    public static final String SHARED_PREFS = "sharedPrefs";
    public String[] listActivite;

    public workerAllActivite(Context c){
        this.c = c;
    }

    @Override
    protected void onPreExecute(){
        this.ad = new AlertDialog.Builder(this.c).create();
        this.ad.setTitle("Login status");
    }

    @Override
    protected String doInBackground(Object[] param) {
        String cible = "https://rb-magasinscolaire.000webhostapp.com/allactivite.php";
        try{
            URL url = new URL(cible);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            OutputStream outs = con.getOutputStream();
            BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs,"utf-8"));

            String msg = URLEncoder.encode("user","utf-8")+"="+
                    URLEncoder.encode((String)param[0],"utf8")+"&"+
                    URLEncoder.encode("pw","utf-8")+"="+
                    URLEncoder.encode((String)param[1],"utf-8");
            bufw.write(msg);
            bufw.flush();
            bufw.close();
            outs.close();

            InputStream ins = con.getInputStream();
            BufferedReader bufr = new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
            String line;
            StringBuffer sbuff = new StringBuffer();

            while((line = bufr.readLine()) !=null){
                sbuff.append(line + "\n");
            }
            return sbuff.toString();

        } catch (Exception e) {
            return e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(Object o){
        String temp = (String)o;
//        if(!temp.trim().isEmpty()){
            String[] arrOfStr = temp.split(",");
        SharedPreferences sharedPreferences = c.getSharedPreferences("SHARED_PREFS",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("list_key",temp);
        editor.commit();

         //   this.listActivite = arrOfStr;
//
//            this.ad.setMessage(temp);
//            this.ad.show();
//        }
//        else{
//            this.ad.setMessage("something wrong");
//            this.ad.show();
//        }
    }

}
